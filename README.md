# model-struct

## 1、仓库说明

设计模式、数据结构、算法。

## 2、分类文档

- **基础原则**
    
    - [六大设计原则](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M00、设计原则.md)
    
- **创建模式**
    
    - [单例模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M01、单例模式.md)
    - [简单工厂模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M02、简单工厂模式.md)
    - [工厂方法模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M03、工厂方法模式.md)
    - [抽象工厂模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M04、抽象工厂模式.md)
    - [原型模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M05、原型模式.md)
    - [建造者模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M06、建造者模式.md)
    
- **结构模式**
    
    - [代理模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M16、代理模式.md)
    - [外观模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M12、外观模式.md)
    - [适配器模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M07、适配器模式.md)
    - [装饰模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M09、装饰模式.md)
    - [组合模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M10、组合模式.md)
    - [享元模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M18、享元模式.md)
    - [桥接模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M08、桥接模式.md)

- **行为模式**
    
    - [观察者模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M11、观察者模式.md)
    - [模板方法模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M19、模板方法模式.md)
    - [策略模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M22、策略模式.md)
    - [命令模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M20、命令模式.md)
    - [调停者模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M17、调停者模式.md)
    - [备忘录模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M24、备忘录模式.md)
    - [解释器模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M14、解释器模式.md)
    - [迭代器模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M13、迭代器模式.md)
    - [状态模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M21、状态模式.md)
    - [责任链模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M15、责任链模式.md)
    - [访问者模式](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/model/M23、访问者模式.md)
    
### 结构与算法

- **基础概念**

    - [稀疏数组与二维数组](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/S01、稀疏与二维数组.md)
    - [队列和栈结构](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/S02、队列和栈结构.md)
    - [单向链表和双向链表](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/S03、单向双向链表.md)
    - [排序与查找算法](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/S04、排序与查找.md)
    - [二叉树与多叉树](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/S05、二叉树与多叉树.md)

- **应用场景**

    - [RSA算法签名验签流程](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/practic/P01、RSA签名算法.md)
    - [树结构业务应用](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/struct/practic/P02、树结构业务应用.md)

## 3、关于作者

| 有问题加：微信号↓ | 支持关注：公众号↓ |
|----|-----|
| <img width="300px" height="300px" src="https://images.gitee.com/uploads/images/2021/0828/182311_7c8ff7e3_5064118.jpeg"/>   |   <img width="300px" height="300px" src="https://images.gitee.com/uploads/images/2021/0828/182332_f1b13009_5064118.jpeg"/>  |

## 4、仓库整合

| 仓库 | 描述 |
|:---|:---|
| [butte-java](https://gitee.com/cicadasmile/butte-java-note) |Java编程文档整理，基础、架构，大数据 |
| [butte-frame](https://gitee.com/cicadasmile/butte-frame-parent) |微服务组件，中间件，常用功能二次封装 |
| [butte-flyer](https://gitee.com/cicadasmile/butte-flyer-parent) |butte-frame二次浅封装，实践案例 |
| [butte-auto](https://gitee.com/cicadasmile/butte-auto-parent) |Jenkins+Docker+K8S实现自动化持续集成 |
| [java-base](https://gitee.com/cicadasmile/java-base-parent) | Jvm、Java基础、Web编程，JDK源码分析 |
| [model-struct](https://gitee.com/cicadasmile/model-arithmetic-parent) | 设计模式、数据结构、算法 |
| [data-manage](https://gitee.com/cicadasmile/data-manage-parent) | 架构设计，实践，数据管理、工具 |
| [spring-mvc](https://gitee.com/cicadasmile/spring-mvc-parent) | Spring+Mvc框架基础总结 |
| [spring-boot](https://gitee.com/cicadasmile/spring-boot-base) | SpringBoot2基础，应用、配置等 |
| [middle-ware](https://gitee.com/cicadasmile/middle-ware-parent) | SpringBoot2进阶，整合常用中间件 |
| [spring-cloud](https://gitee.com/cicadasmile/spring-cloud-base) | Spring+Ali微服务基础组件用法|
| [cloud-shard](https://gitee.com/cicadasmile/cloud-shard-jdbc) | SpringCloud实现分库分表实时扩容 |
| [husky-cloud](https://gitee.com/cicadasmile/husky-spring-cloud) | SpringCloud综合入门案例 |
| [big-data](https://gitee.com/cicadasmile/big-data-parent) | Hadoop框架，大数据组件，数据服务 |
| [mysql-base](https://gitee.com/cicadasmile/mysql-data-base) | MySQL数据库基础、进阶总结 |
| [linux-system](https://gitee.com/cicadasmile/linux-system-base) | Linux系统基础，环境搭建、配置 |
